import React, {createElement, useRef} from 'react';
import {useFormik} from 'formik';
import MyInput from "../UI/input/MyInput";
import MyButton from "../UI/button/MyButton";
import MyTextarea from "../UI/textarea/MyTextarea";
import {BasicSchema} from "../schemas/BasicSchema";
import './BlockForm.module.css'


const BlockForm = () => {
        const formik = useFormik({
            initialValues: {
                file: '',
                title: '',
                website: '',
                textarea: '',
            },
            validationSchema: BasicSchema,
            onSubmit: values => {
                alert(JSON.stringify(values, null, 2));
                console.log(values)
            },
        })


        return (
            <form
                onSubmit={formik.handleSubmit}>

                <MyInput
                    id="upload-photo"
                    name="file"
                    type="file"
                    accept="image/*"
                    multiple='multiple'
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.file}

                />
                {
                    formik.touched.file && formik.errors.file ? (
                        <div>{formik.errors.file}</div>
                    ) : null
                }

                <MyInput
                    id="title"
                    name="title"
                    type="text"
                    placeholder='title'
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.title}
                />

                {
                    formik.touched.title && formik.errors.title ? (
                        <div>{formik.errors.title}</div>
                    ) : null
                }

                <MyInput
                    id="website"
                    name="website"
                    type="website"
                    placeholder='link'
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.website}
                />
                {
                    formik.touched.website && formik.errors.website ? (
                        <div>{formik.errors.website}</div>
                    ) : null
                }

                <MyTextarea
                    id="textarea"
                    name="textarea"
                    type="textarea"
                    placeholder='text'
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.textarea}
                />
                {
                    formik.touched.textarea && formik.errors.textarea ? (
                        <div>{formik.errors.textarea}</div>
                    ) : null
                }

                <MyButton
                    type="submit"
                    onSubmit={formik.handleSubmit}



                >Create</MyButton>
            </form>
        )
            ;
    }
;

export default BlockForm;
