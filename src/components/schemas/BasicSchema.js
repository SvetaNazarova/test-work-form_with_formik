import React from 'react';
import * as Yup from 'yup';

const linkRules = /((https?):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/
const SUPPORTED_FORMATS = ['image/jpg', 'image/jpeg', 'image/png'];

export const BasicSchema = Yup.object().shape({
    file: Yup.mixed()
        .nullable()
        .required('A file is required')
        .test('fileType', "Your Error Message", value => SUPPORTED_FORMATS.includes(value.type) ),
    title: Yup.string()
        .max(20, 'Must be 20 characters or less')
        .required('Required'),
    website: Yup.string()
        .matches(linkRules, 'Enter correct url!')
        .required('Please enter link'),
    textarea: Yup.string()
        .max(250, 'Must be 20 characters or less')
        .required('Required'),
})
